let number = Number(prompt("Give me a number: "));


console.log("The number you provided is "+ number);


while (number > 50){
	if (number % 10 === 0 && number % 5 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		number -= 5;
	}
	else if(number % 10 !== 0 && number % 5 === 0){
		console.log(number);
		number -= 5;
	}
	else{
		break;
	}
}

console.log("The current value is "+ number + "."+ " Terminating the loop.");


/////////////////////////////////////////////////////////////////////

let word = "supercalifragilisticexpialidocious";
let finalWord = "";
let i = 0;

console.log(word);


while (i < word.length){
	if (
		word[i] == "a" ||
		word[i] == "e" ||
		word[i] == "i" ||
		word[i] == "o" ||
		word[i] == "u"
		){
			i++;
		}
	else{
			finalWord += word[i]
			i++;

		}
}


console.log(finalWord);
